# FELIX distribution documentation

## Introduction

The FELIX software is split up into projects, each with its own git repository. The full FELIX software distribution is contained in the felix-distribution project which references all needed projects as git submodules. Felix-distribution also contains scripts to create tar, rpm and help files for fully built distributions.

**Current supported configuration:**
`x86_64-el9-gcc13-opt` (AlmaLinux9, RHEL9)

## How to build felix-distribution

* Clone this repository

  ```
  git clone felix-distribution
  cd felix-distribution
  ```
  > By default the master branch of felix-distribution is cloned. To change branch now run `git checkout <name of desired branch>`

* Populate submodules

  ```
  git submodule init
  git submodule update
  ```
  > Submodule versions correspond to the commits listed in the branch of felix-distribution. To update all submodules to their latest versions run the scripts `./checkout_all.sh <branch name e.g. master>` and `./pull_all.sh <branch name e.g. master>`.

* Prepare the environment

  ```
  source python_env/bin/activate
  source cmake_tdaq/bin/setup.sh
  ```
  > By default Phase-II firmware (rm-5.X.Y) is assumed. To compile software for Phase-I (rm-4.X.Y) firmware now run: `export REGMAP_VERSION=0x0400`


* Compile
  ```
  cmake_config
  cd x86_64-el9-gcc13-opt
  make -j8
  ```


To set up FELIX software in another session, after the first compilation:
```
source felix-distribution/setup.sh
```

## Building a release tar
A release tar can be built using [build-tar.sh](build-tar.sh) script.
1. build `felix-distribution` as described in [the previous section](#how-to-build-felix-distribution).
2. execute the following commands

```
cp credentials_example.sh credentials.sh
# now fill credentials.sh with your CERN account and password
./build-tar.sh <tag> <existing destination folder e.g. ../build/>
```

Examples of `<tag>` are `felix-99-00-00-test`,  `felix-99-00-00-myfix`, `felix-99-00-00-custom`, ...


## Running CI tests locally

CI tests use the CTest framework provided by CMake.
[Having built felix-distribution](#how-to-build-felix-distribution), CI tests are run with:
```
ctest -R <regular expression for test name> -j <number of tests in parallel e.g. 6>
```
This is meant only for developers as some tests require a certain hardware configuration.


## Building individual projects

After the first build of felix-distribution, it is possible to rebuild individual projects.
For example, if the project under development is `felix-star`:
```
cd x86_64-el9-gcc13-opt/felix-star
make
```

## Developing individual projects

If you wish to develop an individual project independently, the clone and build procedure is the same as for felix-distribution, as shown below taking as example `felix-star`.

    git clone felix-star
    cd felix-star
    git submodule init
    git submodule update
    source python_env/bin/activate
    source cmake_tdaq/bin/setup.sh
    cmake_config
    cd x86_64-el9-gcc13-opt
    make -j8

## Updating submodules

Both felix-distribution and the individual projects themselves contain submodules. Should you wish to update the version of a submodule in use, this can be done as follows:

    cd <submodule>
    git checkout master
    git pull
    cd ..

    # try to build and test in x86_64-el9-gcc13-opt

    git commit -a -m "Updated <submodule>"



## Listing of current projects

#### cmake_tdaq

General module to setup for compilation and generate the proper makefiles from cmake. This module contains the versions of all LCG and external modules to be used in cmake_tdaq/cmake/modules/FELIX-versions.cmake.

#### data_transfer_tools

Python scripts to generate block-files of different types and with different data.

#### elinkconfig

GUI to setup elinks. Reads or writes a felix device and reads or writes elink configuration files. These files can also be used with the feconf utility from the ftools project.

#### felig-tools

Tools to control the felig (generator) on a felix device.

#### felix-bus-fs

Second generation bus which uses the filesystem for communication. In use by felix-star as of version 4.2.

#### felix-client

Client to felixbus-fs and netio-next implementing felix-interface.

#### felix-client-thread

Implementation of client which searches for the shared library of felix-client and its dependencies and loads it. A similar module exists in the TDAQ. They can this way just put a felix-client in their LD_LIBRARY_PATH and find that one, independent of the version, as long as the felix-interface has not changed. This module also contains the felix_client_thread_extension which contains methods not yet in the public interface.

#### felix-def

All cross-project definitions in C, C++ and python for FELIX. Includes ports, elinks, and conversions of fids.

#### felix-interface

The actual interface for felix-client. This module is shared with TDAQ. It contains the actual interface. For any new, but not published, methods for the next version, see felix-client-thread. The latter methods can be used by FELIX itself, just not by the TDAQ until they are published.

#### felix-release-notes

Utility to extract ReleaseNotes from JIRA into html format. Used by the FELIX website and to make the release.

#### felix-star

Event-driven, single-threaded, readout application. Replaces felixcore.

#### felix-tag

Module to inject the same version number in all of the programs that depend on this module.

#### felix-unit-test

Unit tests support in Python and C++ to support running tests using supervisord.

#### flxcard

API and low-level tools for the FELIX card.

### flxcard_py

Python interface to flxcard.

#### ftools

High level tools for the FELIX card.

#### hdlc_coder

Module to handle HDLC encoding and decoding (parts that are not in the firmware).

#### netio-next

Event-driven RDMA communication library. Replaces netio.

#### python_env

Python environment for FELIX. Assumes a proper python3 version is picked up from LCG. All extra libraries are installed here.

#### regmap

Register map generated code. Uses wuppercodegen and registermaps in the firmware repository to create C header files.

#### tdaq_tools

Additional tools to include in the felix-distribution if there is not TDAQ installed.

#### wuppercodegen

Tool to generate VHDL, C code, HTML and LaTeX documentation from a single Yaml registermap file.

## Listing of External Modules

All the external libraries and tools to be used by FELIX. No branches here, but different versions on 'master'. Binaries and Libraries are checked into git for these modules.

#### external/bootstrap

Styling library for web pages. In use by statistics module in felixcore and felix-star.

#### external/catch

Testing framework for C++.

#### external/datatables

Table widget for web pages. In use by statistics module in felixcore and felix-star.

#### external/docopt

C++ version of docopt command line parsing.

#### external/highcharts

Graphing widget for web pages. In use by statistics module in felixcore and felix-star.

#### external/jquery

Framework for webapps. In use by statistics module in felixcore and felix-star.

#### external/json

Json reader and writer in C++.

#### external/jwrite

Fast json writer in C.

#### external/libfabric

RDMA support for mellanox cards. Used by netio and netio-next.

#### external/libnuma

Numa support for applications.

#### external/mathjs

Mathematics library in JavaScript.

#### external/patchelf

Utility to patch binaries and libraries. Used to change the RPATH.

#### external/pybind11

Python binding for C++ libraries.

#### external/simdjson

Very fast json reader in C.

#### external/yaml-cpp

Yaml parser in C++.

## Listing of Legacy Modules, to be removed for regmap 5.0

#### felixbase

Base library for felixcore and felixbus.

#### felixbus

Communication Bus to handle lookups of elinks/fids and provide ip and port information. Based on ZeroMQ and Zyre, being replaced by felix-bus-fs.

#### felixbus-client

Client application for the felixbus.

#### felixcore

Multi-threaded readout application for FELIX cards. This application is to be replaced by the single-threaded event driver version felix-star.

#### felixpy

Python interface to felixcore and some of the ftools.

#### netio

Communication library, being replaced by the event-driven netio-next library.

#### packetformat

Module to decode the FELIX block format(s). In use by felixcore. Felix-star has its own internal functions to decode the blocks.

## Listing of Legacy External Modules, to be removed for regmap 5.0

#### external/concurrentqueue

Queue system in use by felixcore.

#### external/czmq

C interface to ZeroMQ.

#### external/simplewebserver

Simple web server in C++ in use by felixcore for statistics.

#### external/spdlog

Logging system in use by felixcore.

#### external/zyre

Broadcast system on top of ZeroMQ and czmq, in use by felixbus.
