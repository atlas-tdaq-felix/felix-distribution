#!/usr/bin/env pytest

import re

from tag_decoder import prefix_dot_dash


def test_regmap():
    (name, dot, dash) = prefix_dot_dash("0x0400")
    assert name == 'felix'
    assert re.match(r"4\.\d+\.\d+", dot)
    assert re.match(r"04\-\d\d\-\d\d", dash)

    (name, dot, dash) = prefix_dot_dash("0x0500")
    assert name == 'felix'
    assert re.match(r"5\.\d+\.\d+", dot)
    assert re.match(r"05\-\d\d\-\d\d", dash)


def test_name():
    (name, dot, dash) = prefix_dot_dash("master")
    assert name == 'felix'
    assert dot == 'master'
    assert dash == 'master'


def test_name_rm():
    (name, dot, dash) = prefix_dot_dash("master-rm5")
    assert name == 'felix'
    assert dot == 'master-rm5'
    assert dash == 'master-rm5'


def test_dashed_name():
    (name, dot, dash) = prefix_dot_dash("master-latest")
    assert name == 'felix'
    assert dot == 'master-latest'
    assert dash == 'master-latest'


def test_dashed_name_rm():
    (name, dot, dash) = prefix_dot_dash("master-latest-rm5")
    assert name == 'felix'
    assert dot == 'master-latest-rm5'
    assert dash == 'master-latest-rm5'


def test_felix_dashed_name():
    (name, dot, dash) = prefix_dot_dash("felix-master")
    assert name == 'felix'
    assert dot == 'master'
    assert dash == 'master'


def test_felix_dashed_name_rm():
    (name, dot, dash) = prefix_dot_dash("felix-master-rm5")
    assert name == 'felix'
    assert dot == 'master-rm5'
    assert dash == 'master-rm5'


def test_prefix_duplets():
    (name, dot, dash) = prefix_dot_dash("test-04-02")
    assert name == 'test'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'

    (name, dot, dash) = prefix_dot_dash("test-4.2")
    assert name == 'test'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'


def test_prefix_duplets_rm():
    (name, dot, dash) = prefix_dot_dash("test-04-02-rm5")
    assert name == 'test'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'

    (name, dot, dash) = prefix_dot_dash("test-4.2-rm5")
    assert name == 'test'  # FIX  'test'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'


def test_duplets():
    (name, dot, dash) = prefix_dot_dash("04-02")
    assert name == 'felix'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'

    (name, dot, dash) = prefix_dot_dash("4.2")
    assert name == 'felix'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'


def test_duplets_rm():
    (name, dot, dash) = prefix_dot_dash("04-02-rm5")
    assert name == 'felix'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'

    (name, dot, dash) = prefix_dot_dash("4.2-rm5")
    assert name == 'felix'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'


def test_prefix_triplet():
    (name, dot, dash) = prefix_dot_dash("test-04-02-00")
    assert name == 'test'
    assert dot == '4.2.0'
    assert dash == '04-02-00'

    (name, dot, dash) = prefix_dot_dash("test-4.2.0")
    assert name == 'test'
    assert dot == '4.2.0'
    assert dash == '04-02-00'


def test_prefix_triplet_rm():
    (name, dot, dash) = prefix_dot_dash("test-04-02-00-rm5")
    assert name == 'test'
    assert dot == '4.2.0-rm5'
    assert dash == '04-02-00-rm5'

    (name, dot, dash) = prefix_dot_dash("test-4.2.0-rm5")
    assert name == 'test'
    assert dot == '4.2.0-rm5'
    assert dash == '04-02-00-rm5'


def test_triplet():
    (name, dot, dash) = prefix_dot_dash("04-02-00")
    assert name == 'felix'
    assert dot == '4.2.0'
    assert dash == '04-02-00'

    (name, dot, dash) = prefix_dot_dash("4.2.0")
    assert name == 'felix'
    assert dot == '4.2.0'
    assert dash == '04-02-00'


def test_triplet_rm():
    (name, dot, dash) = prefix_dot_dash("04-02-00-rm5")
    assert name == 'felix'
    assert dot == '4.2.0-rm5'
    assert dash == '04-02-00-rm5'

    (name, dot, dash) = prefix_dot_dash("4.2.0-rm5")
    assert name == 'felix'
    assert dot == '4.2.0-rm5'
    assert dash == '04-02-00-rm5'


def test_triplet_x():
    (name, dot, dash) = prefix_dot_dash("04-02-xx")
    assert name == 'felix'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'

    (name, dot, dash) = prefix_dot_dash("4.2.x")
    assert name == 'felix'
    assert dot == '4.2.x'
    assert dash == '04-02-xx'


def test_triplet_x_rm():
    (name, dot, dash) = prefix_dot_dash("04-02-xx-rm5")
    assert name == 'felix'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'

    (name, dot, dash) = prefix_dot_dash("4.2.x-rm5")
    assert name == 'felix'
    assert dot == '4.2.x-rm5'
    assert dash == '04-02-xx-rm5'


def test_triplet_rc():
    (name, dot, dash) = prefix_dot_dash("04-02-00-rc3")
    assert name == 'felix'
    assert dot == '4.2.0.rc3'
    assert dash == '04-02-00-rc3'

    (name, dot, dash) = prefix_dot_dash("4.2.0.rc3")
    assert name == 'felix'
    assert dot == '4.2.0.rc3'
    assert dash == '04-02-00-rc3'


def test_triplet_rc_rm():
    (name, dot, dash) = prefix_dot_dash("04-02-00-rc3-rm5")
    assert name == 'felix'
    assert dot == '4.2.0.rc3-rm5'
    assert dash == '04-02-00-rc3-rm5'

    (name, dot, dash) = prefix_dot_dash("4.2.0.rc3-rm5")
    assert name == 'felix'
    assert dot == '4.2.0.rc3-rm5'
    assert dash == '04-02-00-rc3-rm5'


def test_prefix_triplet_rc():
    (name, dot, dash) = prefix_dot_dash("test-04-02-00-rc3")
    assert name == 'test'
    assert dot == '4.2.0.rc3'
    assert dash == '04-02-00-rc3'

    (name, dot, dash) = prefix_dot_dash("test-4.2.0.rc3")
    assert name == 'test'
    assert dot == '4.2.0.rc3'
    assert dash == '04-02-00-rc3'


def test_prefix_triplet_rc_rm():
    (name, dot, dash) = prefix_dot_dash("test-04-02-00-rc3-rm5")
    assert name == 'test'
    assert dot == '4.2.0.rc3-rm5'
    assert dash == '04-02-00-rc3-rm5'

    (name, dot, dash) = prefix_dot_dash("test-4.2.0.rc3-rm5")
    assert name == 'test'
    assert dot == '4.2.0.rc3-rm5'
    assert dash == '04-02-00-rc3-rm5'


def test_prefix_large_triplet():
    (name, dot, dash) = prefix_dot_dash("test-04-32-00")
    assert name == 'test'
    assert dot == '4.32.0'
    assert dash == '04-32-00'

    (name, dot, dash) = prefix_dot_dash("test-4.32.0")
    assert name == 'test'
    assert dot == '4.32.0'
    assert dash == '04-32-00'


def test_prefix_large_triplet_rm():
    (name, dot, dash) = prefix_dot_dash("test-04-32-00-rm5")
    assert name == 'test'
    assert dot == '4.32.0-rm5'
    assert dash == '04-32-00-rm5'

    (name, dot, dash) = prefix_dot_dash("test-4.32.0-rm5")
    assert name == 'test'
    assert dot == '4.32.0-rm5'
    assert dash == '04-32-00-rm5'


def test_large_triplet():
    (name, dot, dash) = prefix_dot_dash("04-32-00")
    assert name == 'felix'
    assert dot == '4.32.0'
    assert dash == '04-32-00'

    (name, dot, dash) = prefix_dot_dash("4.32.0")
    assert name == 'felix'
    assert dot == '4.32.0'
    assert dash == '04-32-00'


def test_large_triplet_rm():
    (name, dot, dash) = prefix_dot_dash("04-32-00-rm5")
    assert name == 'felix'
    assert dot == '4.32.0-rm5'
    assert dash == '04-32-00-rm5'

    (name, dot, dash) = prefix_dot_dash("4.32.0-rm5")
    assert name == 'felix'
    assert dot == '4.32.0-rm5'
    assert dash == '04-32-00-rm5'


def test_large_triplet_rc():
    (name, dot, dash) = prefix_dot_dash("04-32-00-rc3")
    assert name == 'felix'
    assert dot == '4.32.0.rc3'
    assert dash == '04-32-00-rc3'

    (name, dot, dash) = prefix_dot_dash("4.32.0.rc3")
    assert name == 'felix'
    assert dot == '4.32.0.rc3'
    assert dash == '04-32-00-rc3'


def test_large_triplet_rc_rm():
    (name, dot, dash) = prefix_dot_dash("04-32-00-rc3-rm5")
    assert name == 'felix'
    assert dot == '4.32.0.rc3-rm5'
    assert dash == '04-32-00-rc3-rm5'

    (name, dot, dash) = prefix_dot_dash("4.32.0.rc3-rm5")
    assert name == 'felix'
    assert dot == '4.32.0.rc3-rm5'
    assert dash == '04-32-00-rc3-rm5'


def test_prefix_large_triplet_rc():
    (name, dot, dash) = prefix_dot_dash("test-04-32-00-rc3")
    assert name == 'test'
    assert dot == '4.32.0.rc3'
    assert dash == '04-32-00-rc3'

    (name, dot, dash) = prefix_dot_dash("test-4.32.0.rc3")
    assert name == 'test'
    assert dot == '4.32.0.rc3'
    assert dash == '04-32-00-rc3'


def test_prefix_large_triplet_rc_rm():
    (name, dot, dash) = prefix_dot_dash("test-04-32-00-rc3-rm5")
    assert name == 'test'
    assert dot == '4.32.0.rc3-rm5'
    assert dash == '04-32-00-rc3-rm5'

    (name, dot, dash) = prefix_dot_dash("test-4.32.0.rc3-rm5")
    assert name == 'test'
    assert dot == '4.32.0.rc3-rm5'
    assert dash == '04-32-00-rc3-rm5'


def test_relnotes():
    (name, dot, dash) = prefix_dot_dash("test-04-02-07")
    assert name == 'test'
    assert dot == '4.2.7'
    assert dash == '04-02-07'

    (name, dot, dash) = prefix_dot_dash("test-04-02-07-rc1")
    assert name == 'test'
    assert dot == '4.2.7.rc1'
    assert dash == '04-02-07-rc1'

    (name, dot, dash) = prefix_dot_dash("test-04-02-07-beta1")
    assert name == 'test'
    assert dot == '4.2.7.beta1'
    assert dash == '04-02-07-beta1'

    (name, dot, dash) = prefix_dot_dash("test-04-02-06-7-g91a0383")
    assert name == 'test'
    assert dot == '4.2.7'
    assert dash == '04-02-07'

    (name, dot, dash) = prefix_dot_dash("test-04-02-07-rc3-7-g91a0383")
    assert name == 'test'
    assert dot == '4.2.7.rc3'
    assert dash == '04-02-07-rc3'

    (name, dot, dash) = prefix_dot_dash("test-04-02-07-beta3-7-g91a0383")
    assert name == 'test'
    assert dot == '4.2.7.beta3'
    assert dash == '04-02-07-beta3'
