# Usage: source setup.sh
export FI_VERBS_TX_IOV_LIMIT=30
export FI_VERBS_RX_IOV_LIMIT=30

BINARY_TAG=${BINARY_TAG:=x86_64-el9-gcc13-opt}
LCG_VERSION=106b
PYTHON_VERSION=3.11.9
PYTHON_LIB_VERSION=python3.11

if [[ "$#" -eq 0 || $1 != "-q" ]]; then
  echo "Setting up FELIX (user)"
fi

# making sure stack size is ok
felix_stack_size=`ulimit -s`
felix_min_stack_size=10240
if [ $felix_stack_size != "unlimited" ] && [ $felix_stack_size -lt $felix_min_stack_size ]; then
    ulimit -s $felix_min_stack_size;
fi

LCG_BASE=${LCG_BASE:=/cvmfs/sft.cern.ch/lcg}
if [ ! -d "$LCG_BASE" ]; then
  echo "Did not find $LCG_BASE"
  LCG_BASE=/sw/atlas/sw/lcg
  if [ ! -d "$LCG_BASE" ]; then
    echo "Did not find $LCG_BASE either"
    echo "ERROR: FELIX not setup"
    return
  fi
fi
echo "Using $LCG_BASE"

export PATH=${LCG_BASE}/releases/LCG_${LCG_VERSION}/Python/${PYTHON_VERSION}/${BINARY_TAG}/bin:${PATH}

export FELIX_ROOT=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))
export PATH=${FELIX_ROOT}/bin:${PATH}
export PYTHONPATH=${FELIX_ROOT}/python:${PYTHONPATH}
export PYTHONPATH=${FELIX_ROOT}/lib/:${PYTHONPATH}
export PYTHONPATH=${FELIX_ROOT}/lib/${PYTHON_LIB_VERSION}/site-packages:${PYTHONPATH}
export QT_XKB_CONFIG_ROOT=${FELIX_ROOT}/bin/xkb
export LD_LIBRARY_PATH=${FELIX_ROOT}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${FELIX_ROOT}/lib/${PYTHON_LIB_VERSION}/site-packages/cffi.libs/:${LD_LIBRARY_PATH}
