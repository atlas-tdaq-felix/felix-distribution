This is the FELIX software distribution which includes:

- the felix-star application
- the felixcore application
- the ftools to access the card
- elinkconfig to configure the card
- include files and libraries to use the felix api

Instructions
------------
```
tar zxvf felix-xx-yy-zz-stand-alone-<binary-tag>.tar.gz
source felix-xx-yy-zz-stand-alone_vv/<binary-tag>/setup.sh
```

tools are then available on your PATH

The driver for FELIX is packaged separately.

# Required packages for Alma9

you will need to install the following packages:

```
sudo yum install gcc
sudo yum install mesa-libGL
sudo yum install libpng
sudo yum install libSM
sudo yum install libXrender
sudo yum install libXdamage
sudo yum install fontconfig
sudo yum install librdmacm
sudo yum install ibverbs
```

--
Contact:

Mark Donszelmann, Mark.Donszelmann@cern.ch
