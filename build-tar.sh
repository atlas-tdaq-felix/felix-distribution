#!/bin/sh -x
set -e

usage()
{
  echo "usage: $(basename $0) <name> [<destination_dir>]"
  exit 1
}

if [ $# -lt 1 ]; then
    usage
fi

DRIVER_VERSIONS="felix-drivers-04-20-00 felix-drivers-04-19-00"

TARGET_DIR=tmp-build-lib
TYPE=stand-alone
NAME=${1}
DIST_NAME=${NAME}-${TYPE}
TARGET_NAME=$TARGET_DIR/${DIST_NAME}
shift
# BINARY_TAG -- given in cmake_tdaq script
# LCG_VERSION -- given in cmake_tdaq script
# LCG_QT_VERSION -- given in cmake_tdaq script
LCG_FT_VERSION=2.10
DST_DIR=.
if [ $# -ge 1 ]; then
  DST_DIR=${1}
  shift
fi

DOT_VERSION=`./tag-decode --dot ${NAME}`
echo ${DOT_VERSION}
if [[ ${DOT_VERSION} =~ ^[0-9]+\.[0-9]+\.[0-9x]+.*$ ]]; then
    echo "Valid version ${DOT_VERSION}"
else
    echo "Invalid version ${DOT_VERSION}, should be of the form: felix-04-01-00-rc6 or felix-04-01-00, probably a test or nightly"
fi


SRC=${BINARY_TAG}
DST=${TARGET_NAME}/${BINARY_TAG}

VERSIONS=${DST}/versions.txt
CVMFS_VERSIONS=${DST}/versions-cvmfs.txt
EXTERNAL_VERSIONS=${DST}/versions-external.txt
PATCHELF=external/patchelf/0.12/x86_64-centos7-gcc8-opt/bin/patchelf

BIN=${TARGET_NAME}/${BINARY_TAG}/bin
PYTHON=${TARGET_NAME}/${BINARY_TAG}/python
LIB=${TARGET_NAME}/${BINARY_TAG}/lib
LIB_PYTHON=${TARGET_NAME}/${BINARY_TAG}/lib/python3.11/site-packages
INC=${TARGET_NAME}/${BINARY_TAG}/include
SHR=${TARGET_NAME}/${BINARY_TAG}/share
ETC=${TARGET_NAME}/${BINARY_TAG}/etc
TST=${TARGET_NAME}/${BINARY_TAG}/test

PLATFORMS=${BIN}/platforms
XKB=${BIN}/xkb

#DRV=${TARGET_NAME}/${BINARY_TAG}/drivers

mkdir -p ${DST}
mkdir -p ${BIN}
mkdir -p ${PYTHON}
mkdir -p ${LIB}
mkdir -p ${LIB_PYTHON}
mkdir -p ${INC}
mkdir -p ${SHR}
mkdir -p ${ETC}
mkdir -p ${TST}
#mkdir -p ${DRV}

# general
( cd cmake_tdaq; echo -n "${PWD##*/} " > ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
( echo -n "${PWD##*/} " >> ${VERSIONS}; git describe --tags --always --long >> ${VERSIONS} )
cp src/README.md ${TARGET_NAME}
cp src/${BINARY_TAG}/setup.sh ${TARGET_NAME}/${BINARY_TAG}/
cp src/bin/* ${BIN}
cp src/test/* ${TST}

#
# Release Notes
#
test -f ./tokens.sh && source ./tokens.sh
GIT_DESCRIBE_VERSION=`git describe --tags`
RELNOTES_VERSION=`./tag-decode --dash-tag ${GIT_DESCRIBE_VERSION}`
echo ${RELNOTES_VERSION}
./felix-release-notes/generate-release-notes.py ${RELNOTES_VERSION} ${DRIVER_VERSIONS} --notepath=./felix-release-notes/notes > ${DST}/ReleaseNotes.html

# QT5 loaded at runtime
mkdir -p ${PLATFORMS}
find -L ${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/qt5 | grep libqxcb\.so | grep ${BINARY_TAG} | xargs cp -P -t ${PLATFORMS}
find -L ${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/qt5 | grep libQt5DBus\.so\.5 | grep ${BINARY_TAG} | xargs cp -P -t ${LIB}
# QT5 needed
mkdir -p ${XKB}
cp -r /usr/share/X11/xkb/* ${XKB}

# FreeType needed by QT5 at runtime, for LCG > 91
find -L ${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/freetype | grep libfreetype\.so | grep ${BINARY_TAG} | xargs -r cp -P -t ${LIB}

# mesa-libGL needed if not installed on netbooted clients
find -L /usr/lib64/libglapi.* | xargs cp -P -t ${LIB}
find -L /usr/lib64/libGL.* | xargs cp -P -t ${LIB}
find -L /usr/lib64/libGLX.* | xargs cp -P -t ${LIB}
find -L /usr/lib64/libGLdispatch.* | xargs cp -P -t ${LIB}

# copy missing libX11 libs
find -L /usr/lib64/libXdamage.* | xargs cp -P -t ${LIB}

# Copy external include files
cp -r ${SRC}/include/* ${INC}

#
# for regmap 4 only (DISABLED)
#
#if [ "$REGMAP_VERSION" == "0x0400" ]; then

    # felixbase
    ( cd felixbase; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp -r felixbase/felixbase ${INC}
    cp ${SRC}/felixbase/libfelixbase.so ${LIB}/libfelixbase.so.${DOT_VERSION}
    ln -s libfelixbase.so.${DOT_VERSION} ${LIB}/libfelixbase.so
    cp ${SRC}/felixbase/libfelixbase.so.debug ${LIB}/libfelixbase.so.debug.${DOT_VERSION}
    ln -s libfelixbase.so.debug.${DOT_VERSION} ${LIB}/libfelixbase.so.debug


    # felixbus
    ( cd felixbus; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp -r felixbus/felixbus ${INC}
    cp ${SRC}/felixbus/libfelixbus.so ${LIB}/libfelixbus.so.${DOT_VERSION}
    ln -s libfelixbus.so.${DOT_VERSION} ${LIB}/libfelixbus.so
    cp ${SRC}/felixbus/libfelixbus.so.debug ${LIB}/libfelixbus.so.debug.${DOT_VERSION}
    ln -s libfelixbus.so.debug.${DOT_VERSION} ${LIB}/libfelixbus.so.debug
    cp ${SRC}/felixbus/libfelixbus_py.so ${LIB}/libfelixbus_py.so.${DOT_VERSION}
    ln -s libfelixbus_py.so.${DOT_VERSION} ${LIB}/libfelixbus_py.so
    cp ${SRC}/felixbus/libfelixbus_py.so.debug ${LIB}/libfelixbus_py.so.debug.${DOT_VERSION}
    ln -s libfelixbus_py.so.debug.${DOT_VERSION} ${LIB}/libfelixbus_py.so.debug


    # felixbus-client
    ( cd felixbus-client; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp ${SRC}/felixbus-client/felix-buslist ${BIN}
    cp ${SRC}/felixbus-client/felix-buslist.debug ${BIN}

    # felixcore
    ( cd felixcore; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp ${SRC}/felixcore/felixcore ${BIN}
    cp ${SRC}/felixcore/felixcore.debug ${BIN}
    cp ${SRC}/felixcore/felix-client ${BIN}
    cp ${SRC}/felixcore/felix-client.debug ${BIN}
    cp ${SRC}/felixcore/felix-dcs ${BIN}
    # FIXME
    #cp ${SRC}/felixcore/felix-dcs.debug ${BIN}
    cp ${SRC}/felixcore/libfelix.so ${LIB}/libfelix.so.${DOT_VERSION}
    ln -s libfelix.so.${DOT_VERSION} ${LIB}/libfelix.so
    cp ${SRC}/felixcore/libfelix.so.debug ${LIB}/libfelix.so.debug.${DOT_VERSION}
    ln -s libfelix.so.debug.${DOT_VERSION} ${LIB}/libfelix.so.debug
    mkdir -p ${SHR}/felixcore/web
    cp -r ${SRC}/felixcore/share/felixcore/web/* ${SHR}/felixcore/web

    # netio
    ( cd netio; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp netio/netio/netio.hpp ${INC}
    cp ${SRC}/netio/netio_cat ${BIN}
    cp ${SRC}/netio/netio_cat.debug ${BIN}
    cp ${SRC}/netio/netio_data_pub ${BIN}
    cp ${SRC}/netio/netio_data_pub.debug ${BIN}
    cp ${SRC}/netio/netio_pingpong ${BIN}
    cp ${SRC}/netio/netio_pingpong.debug ${BIN}
    #cp ${SRC}/netio/netio_pubsub ${BIN}
    #cp ${SRC}/netio/netio_pubsub.debug ${BIN}
    cp ${SRC}/netio/netio_recv ${BIN}
    cp ${SRC}/netio/netio_recv.debug ${BIN}
    cp ${SRC}/netio/netio_test ${BIN}
    cp ${SRC}/netio/netio_test.debug ${BIN}
    cp ${SRC}/netio/netio_throughput ${BIN}
    cp ${SRC}/netio/netio_throughput.debug ${BIN}
    cp ${SRC}/netio/libnetio.so ${LIB}/libnetio.so.${DOT_VERSION}
    ln -s libnetio.so.${DOT_VERSION} ${LIB}/libnetio.so
    cp ${SRC}/netio/libnetio.so.debug ${LIB}/libnetio.so.debug.${DOT_VERSION}
    ln -s libnetio.so.debug.${DOT_VERSION} ${LIB}/libnetio.so.debug

    # packetformat
    ( cd packetformat; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
    cp -r packetformat/packetformat ${INC}
    cp ${SRC}/packetformat/libpacketformat.so ${LIB}/libpacketformat.so.${DOT_VERSION}
    ln -s libpacketformat.so.${DOT_VERSION} ${LIB}/libpacketformat.so
    cp ${SRC}/packetformat/libpacketformat.so.debug ${LIB}/libpacketformat.so.debug.${DOT_VERSION}
    ln -s libpacketformat.so.debug.${DOT_VERSION} ${LIB}/libpacketformat.so.debug

#fi

#
# Applications
#
cp external/xilinx-xvc/xvcserver/${BINARY_TAG}/bin/xvc_pcie ${BIN}

#
# Rest ALPHABETICAL
#

# data_transfer_tools
# ( cd data_transfer_tools; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
# mkdir -p ${TST}/data_transfer_tools/
# cp data_transfer_tools/scripts/data_transfer_tools-test ${TST}/data_transfer_tools/
# cp -r ${SRC}/data_transfer_tools/test ${TST}/data_transfer_tools

# elinkconfig
( cd elinkconfig; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/elinkconfig/elinkconfig ${BIN}
cp ${SRC}/elinkconfig/elinkconfig.debug ${BIN}


# fatcat
#( cd fatcat; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
#cp ${SRC}/fatcat/fatcat ${BIN}
#cp ${SRC}/fatcat/fatcat.debug ${BIN}

# felig-tools
( cd felig-tools; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/felig-tools/felig-config ${BIN}
cp ${SRC}/felig-tools/felig-config.debug ${BIN}
cp ${SRC}/felig-tools/felig-start ${BIN}
cp ${SRC}/felig-tools/felig-start.debug ${BIN}
cp ${SRC}/felig-tools/felig-stop ${BIN}
cp ${SRC}/felig-tools/felig-stop.debug ${BIN}
cp ${SRC}/felig-tools/felig-status ${BIN}
cp ${SRC}/felig-tools/felig-status.debug ${BIN}
cp ${SRC}/felig-tools/felig-reset ${BIN}
cp ${SRC}/felig-tools/felig-reset.debug ${BIN}

# felix-bus-fs
( cd felix-bus-fs; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r ${SRC}/felix-bus-fs/include/* ${INC}
cp ${SRC}/felix-bus-fs/libfelix-bus-lib.so ${LIB}/libfelix-bus-lib.so.${DOT_VERSION}
ln -s libfelix-bus-lib.so.${DOT_VERSION} ${LIB}/libfelix-bus-lib.so
cp ${SRC}/felix-bus-fs/libfelix-bus-lib.so.debug ${LIB}/libfelix-bus-lib.so.debug.${DOT_VERSION}
ln -s libfelix-bus-lib.so.debug.${DOT_VERSION} ${LIB}/libfelix-bus-lib.so.debug
cp ${SRC}/felix-bus-fs/libfelix-bus-server-lib.so ${LIB}/libfelix-bus-server-lib.so.${DOT_VERSION}
ln -s libfelix-bus-server-lib.so.${DOT_VERSION} ${LIB}/libfelix-bus-server-lib.so
cp ${SRC}/felix-bus-fs/libfelix-bus-server-lib.so.debug ${LIB}/libfelix-bus-server-lib.so.debug.${DOT_VERSION}
ln -s libfelix-bus-server-lib.so.debug.${DOT_VERSION} ${LIB}/libfelix-bus-server-lib.so.debug
cp ${SRC}/felix-bus-fs/libfelix_bus_py.so ${LIB}/libfelix_bus_py.so.${DOT_VERSION}
ln -s libfelix_bus_py.so.${DOT_VERSION} ${LIB}/libfelix_bus_py.so
cp ${SRC}/felix-bus-fs/libfelix_bus_py.so.debug ${LIB}/libfelix_bus_py.so.debug.${DOT_VERSION}
ln -s libfelix_bus_py.so.debug.${DOT_VERSION} ${LIB}/libfelix_bus_py.so.debug


# felix-config
#( cd felix-config; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
#cp felix-config/scripts/* ${BIN}
#cp -r felix-config/python/* ${PYTHON}
#cp ${SRC}/felix-config/libFlxCardPy.so ${LIB}/libFlxCardPy.so.${DOT_VERSION}
#ln -s libFlxCardPy.so.${DOT_VERSION} ${LIB}/libFlxCardPy.so
#cp ${SRC}/felix-config/libFlxCardPy.so.debug ${LIB}/libFlxCardPy.so.debug.${DOT_VERSION}
#ln -s libFlxCardPy.so.debug.${DOT_VERSION} ${LIB}/libFlxCardPy.so.debug


# felix-client
( cd felix-client; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
mkdir -p ${INC}/felix
cp felix-client/felix/felix_client.hpp ${INC}/felix
cp felix-client/felix/felix_client_thread_impl.hpp ${INC}/felix
cp felix-client/felix/felix_client_context_handler.hpp ${INC}/felix
cp felix-client/felix/felix_client_info.hpp ${INC}/felix
cp felix-client/felix/felix_client_util.hpp ${INC}/felix
cp ${SRC}/felix-client/libfelix-client-lib.so ${LIB}/libfelix-client-lib.so.${DOT_VERSION}
ln -s libfelix-client-lib.so.${DOT_VERSION} ${LIB}/libfelix-client-lib.so
cp ${SRC}/felix-client/libfelix-client-lib.so.debug ${LIB}/libfelix-client-lib.so.debug.${DOT_VERSION}
ln -s libfelix-client-lib.so.debug.${DOT_VERSION} ${LIB}/libfelix-client-lib.so.debug

mkdir -p ${TST}/felix-client/
cp -r ${SRC}/felix-client/test ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-test ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-subscribe ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-exec ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-loopback ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-sca-loopback ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-sca-commands ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-sca-grouping ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-sca-loopback ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-sca-send ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-mt-sca-loopback ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-vector-loopback ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-send ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-subscribe ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-subscribe-vector ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-mt-subscribe ${TST}/felix-client
cp ${SRC}/felix-client/felix-client-thread-unsubscribe ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-buffered-loopback ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-buffered-loopback-vector ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-buffered-publish ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-buffered-recv ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-unbuffered-loopback-vector ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-unbuffered-publish ${TST}/felix-client
cp ${SRC}/felix-client/netio-bus-unbuffered-recv ${TST}/felix-client


# felix-client-thread
( cd felix-client-thread; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-client-thread/felix ${INC}
cp ${SRC}/felix-client-thread/libfelix-client-thread.so ${LIB}/libfelix-client-thread.so.${DOT_VERSION}
ln -s libfelix-client-thread.so.${DOT_VERSION} ${LIB}/libfelix-client-thread.so
cp ${SRC}/felix-client-thread/libfelix-client-thread.so.debug ${LIB}/libfelix-client-thread.so.debug.${DOT_VERSION}
ln -s libfelix-client-thread.so.debug.${DOT_VERSION} ${LIB}/libfelix-client-thread.so.debug


# felix-def
( cd felix-def; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-def/include/felix ${INC}
cp -r felix-def/python/* ${PYTHON}


# felix-direct-readout-impl
( cd felix-direct-readout-implementation; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/felix-direct-readout-implementation/libfelix-direct-readout-impl-lib.so ${LIB}/libfelix-direct-readout-impl-lib.so.${DOT_VERSION}
cp ${SRC}/felix-direct-readout-implementation/libfelix-direct-readout-impl-lib.so.debug ${LIB}/libfelix-direct-readout-impl-lib.so.debug.${DOT_VERSION}
ln -s libfelix-direct-readout-impl-lib.so.${DOT_VERSION} ${LIB}/libfelix-direct-readout-impl-lib.so
ln -s libfelix-direct-readout-impl-lib.so.debug.${DOT_VERSION} ${LIB}/libfelix-direct-readout-impl-lib.so.debug


# felix-direct-readout-interface
( cd felix-direct-readout-interface; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-direct-readout-interface/felix ${INC}


# felix-interface
( cd felix-interface; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-interface/felix ${INC}


# felix-mapper
( cd felix-mapper; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-mapper/felix-mapper ${INC}
cp ${SRC}/felix-mapper/libfelix-mapper.so ${LIB}/libfelix-mapper.so.${DOT_VERSION}
ln -s libfelix-mapper.so.${DOT_VERSION} ${LIB}/libfelix-mapper.so
cp ${SRC}/felix-mapper/libfelix-mapper.so.debug ${LIB}/libfelix-mapper.so.debug.${DOT_VERSION}
ln -s libfelix-mapper.so.debug.${DOT_VERSION} ${LIB}/libfelix-mapper.so.debug
cp ${SRC}/felix-mapper/felix-mapper-check ${BIN}
cp ${SRC}/felix-mapper/felix-mapper-check.debug ${BIN}

# cp -r ${SRC}/felix-mapper/test ${TST}/felix-mapper


# felix-star
( cd felix-star; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/felix-star/libfelix-star-lib.so ${LIB}/libfelix-star-lib.so.${DOT_VERSION}
ln -s libfelix-star-lib.so.${DOT_VERSION} ${LIB}/libfelix-star-lib.so
cp ${SRC}/felix-star/libfelix-star-lib.so.debug ${LIB}/libfelix-star-lib.so.debug.${DOT_VERSION}
ln -s libfelix-star-lib.so.debug.${DOT_VERSION} ${LIB}/libfelix-star-lib.so.debug
#cp ${SRC}/felix-star/felix-cmem-free ${BIN}
#cp ${SRC}/felix-star/felix-cmem-free.debug ${BIN}
#cp ${SRC}/felix-star/felix-display-stats ${BIN}
cp ${SRC}/felix-star/felix-elink2file ${BIN}
cp ${SRC}/felix-star/felix-fid ${BIN}
cp ${SRC}/felix-star/felix-fid.debug ${BIN}
cp ${SRC}/felix-star/felix-fifo2elink ${BIN}
cp ${SRC}/felix-star/felix-file2host ${BIN}
cp ${SRC}/felix-star/felix-file2host.debug ${BIN}
cp ${SRC}/felix-star/felix-get-ip ${BIN}
#cp ${SRC}/felix-star/felix-get-mode ${BIN}
cp ${SRC}/felix-star/felix-register ${BIN}
cp ${SRC}/felix-star/felix-register.debug ${BIN}
cp ${SRC}/felix-star/felix-register-client ${BIN}
cp ${SRC}/felix-star/felix-register-client.debug ${BIN}
cp ${SRC}/felix-star/felix-test-send-toflx ${BIN}
cp ${SRC}/felix-star/felix-test-send-toflx.debug ${BIN}
cp ${SRC}/felix-star/felix-test-swrod ${BIN}
cp ${SRC}/felix-star/felix-test-swrod.debug ${BIN}
cp ${SRC}/felix-star/felix-test-swrod-mt ${BIN}
cp ${SRC}/felix-star/felix-test-swrod-mt.debug ${BIN}
cp ${SRC}/felix-star/felix-toflx ${BIN}
cp ${SRC}/felix-star/felix-toflx.debug ${BIN}
cp ${SRC}/felix-star/felix-toflx2file ${BIN}
cp ${SRC}/felix-star/felix-toflx2file.debug ${BIN}
cp ${SRC}/felix-star/felix-tohost ${BIN}
cp ${SRC}/felix-star/felix-tohost.debug ${BIN}

cp -r felix-star/python/* ${PYTHON}
# cp -r felix-star/share/* ${SHR}
mkdir -p ${SHR}/felix-star/
cp regmap/src/regmap4-symbol.yaml ${SHR}/felix-star/
cp regmap/src/regmap5-symbol.yaml ${SHR}/felix-star/

mkdir -p ${TST}/felix-star/
cp -r ${SRC}/felix-star/test ${TST}/felix-star
cp ${SRC}/felix-star/felix-star-test ${TST}/felix-star/
cp ${SRC}/felix-star/felix-bus ${TST}/felix-star/
cp ${SRC}/felix-star/felix-stats-generate ${TST}/felix-star/
#cp ${SRC}/felix-star/felix-test-send-toflx-unbuf ${TST}/felix-star/
ln -s ../../bin/felix-elink2file ${TST}/felix-star/
ln -s ../../bin/felix-fid ${TST}/felix-star/
ln -s ../../bin/felix-fifo2elink ${TST}/felix-star/
ln -s ../../bin/felix-file2host ${TST}/felix-star/
ln -s ../../bin/felix-get-ip ${TST}/felix-star/
ln -s ../../bin/felix-get-mode ${TST}/felix-star/
ln -s ../../bin/felix-register-client ${TST}/felix-star/
ln -s ../../bin/felix-register-simulator ${TST}/felix-star/
ln -s ../../bin/felix-test-send-toflx ${TST}/felix-star/
ln -s ../../bin/felix-test-send-toflx-buffered ${TST}/felix-star/
ln -s ../../bin/felix-test-swrod ${TST}/felix-star/
ln -s ../../bin/felix-test-swrod-buffered ${TST}/felix-star/
ln -s ../../bin/felix-test-swrod-unbuf ${TST}/felix-star/
ln -s ../../bin/felix-toflx2file ${TST}/felix-star/


# felix-starter
( cd felix-starter; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/felix-starter/felix-sv-test ${BIN}
cp ${SRC}/felix-starter/felix-sv-test.debug ${BIN}
cp ${SRC}/felix-starter/felix-supervisor ${BIN}
cp ${SRC}/felix-starter/tdaq-felix2atlas ${BIN}
# cp ${SRC}/felix-starter/tdaq-felix-supervisor ${BIN}
cp ${SRC}/felix-starter/felix-supervisord-generate ${BIN}
cp ${SRC}/felix-starter/felix-multivisor ${BIN}
cp -r felix-starter/python/* ${PYTHON}
cp -r felix-starter/etc/* ${ETC}

mkdir -p ${TST}/felix-starter/
cp -r ${SRC}/felix-starter/test ${TST}/felix-starter
ln -s ../../bin/felix-supervisord-generate ${TST}/felix-starter/

# felix-tag
( cd felix-tag; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/felix-tag/felixtag.py ${PYTHON}


# felix-unit-test
( cd felix-unit-test; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r felix-unit-test/python/* ${PYTHON}

mkdir -p ${TST}/felix-unit-test/
cp -r ${SRC}/felix-unit-test/test ${TST}/felix-unit-test/test
cp ${SRC}/felix-unit-test/felix-unit-test-test ${TST}/felix-unit-test/


# flxcard
( cd flxcard; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/flxcard/flx-config ${BIN}
cp ${SRC}/flxcard/flx-config.debug ${BIN}
cp ${SRC}/flxcard/flx-dma-stat ${BIN}
cp ${SRC}/flxcard/flx-dma-stat.debug ${BIN}
cp ${SRC}/flxcard/flx-dma-test ${BIN}
cp ${SRC}/flxcard/flx-dma-test.debug ${BIN}
cp ${SRC}/flxcard/flx-dump-blocks ${BIN}
cp ${SRC}/flxcard/flx-dump-blocks.debug ${BIN}
cp ${SRC}/flxcard/flx-i2c ${BIN}
cp ${SRC}/flxcard/flx-i2c.debug ${BIN}
cp ${SRC}/flxcard/flx-info ${BIN}
cp ${SRC}/flxcard/flx-info.debug ${BIN}
cp ${SRC}/flxcard/flx-init ${BIN}
cp ${SRC}/flxcard/flx-init.debug ${BIN}
cp ${SRC}/flxcard/flx-irq-test ${BIN}
cp ${SRC}/flxcard/flx-irq-test.debug ${BIN}
cp ${SRC}/flxcard/flx-pod ${BIN}
cp ${SRC}/flxcard/flx-pod.debug ${BIN}
cp ${SRC}/flxcard/flx-reset ${BIN}
cp ${SRC}/flxcard/flx-reset.debug ${BIN}
cp ${SRC}/flxcard/flx-throughput ${BIN}
cp ${SRC}/flxcard/flx-throughput.debug ${BIN}
cp ${SRC}/flxcard/flx-busy-mon ${BIN}
cp ${SRC}/flxcard/flx-busy-mon.debug ${BIN}
cp ${SRC}/flxcard/libFlxCard.so ${LIB}/libFlxCard.so.${DOT_VERSION}
ln -s libFlxCard.so.${DOT_VERSION} ${LIB}/libFlxCard.so
cp ${SRC}/flxcard/libFlxCard.so.debug ${LIB}/libFlxCard.so.debug.${DOT_VERSION}
ln -s libFlxCard.so.debug.${DOT_VERSION} ${LIB}/libFlxCard.so.debug


# flxcard_py
( cd flxcard_py; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/flxcard_py/libflxcard_py.so ${LIB}/libflxcard_py.so.${DOT_VERSION}
ln -s libflxcard_py.so.${DOT_VERSION} ${LIB}/libflxcard_py.so
cp ${SRC}/flxcard_py/libflxcard_py.so.debug ${LIB}/libflxcard_py.so.debug.${DOT_VERSION}
ln -s libflxcard_py.so.debug.${DOT_VERSION} ${LIB}/libflxcard_py.so.debug


# ftools
( cd ftools; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp ${SRC}/ftools/fcheck ${BIN}
cp ${SRC}/ftools/fcheck.debug ${BIN}
cp ${SRC}/ftools/fcap ${BIN}
cp ${SRC}/ftools/fcap.debug ${BIN}
cp ${SRC}/ftools/fdaq ${BIN}
cp ${SRC}/ftools/fdaq.debug ${BIN}
cp ${SRC}/ftools/fdaqm ${BIN}
cp ${SRC}/ftools/fdaqm.debug ${BIN}
cp ${SRC}/ftools/febrc ${BIN}
cp ${SRC}/ftools/febrc.debug ${BIN}
cp ${SRC}/ftools/fec ${BIN}
cp ${SRC}/ftools/fec.debug ${BIN}
cp ${SRC}/ftools/feconf ${BIN}
cp ${SRC}/ftools/feconf.debug ${BIN}
cp ${SRC}/ftools/fedma ${BIN}
cp ${SRC}/ftools/fedma.debug ${BIN}
cp ${SRC}/ftools/fedump ${BIN}
cp ${SRC}/ftools/fedump.debug ${BIN}
cp ${SRC}/ftools/fela ${BIN}
cp ${SRC}/ftools/fela.debug ${BIN}
cp ${SRC}/ftools/felink ${BIN}
cp ${SRC}/ftools/felink.debug ${BIN}
cp ${SRC}/ftools/femu ${BIN}
cp ${SRC}/ftools/femu.debug ${BIN}
cp ${SRC}/ftools/femuran ${BIN}
cp ${SRC}/ftools/femuran.debug ${BIN}
cp ${SRC}/ftools/fereverse ${BIN}
cp ${SRC}/ftools/fereverse.debug ${BIN}
cp ${SRC}/ftools/feto ${BIN}
cp ${SRC}/ftools/feto.debug ${BIN}
cp ${SRC}/ftools/fexoff ${BIN}
cp ${SRC}/ftools/fexofftx ${BIN}
cp ${SRC}/ftools/fflash ${BIN}
cp ${SRC}/ftools/fflash.debug ${BIN}
cp ${SRC}/ftools/fflashprog ${BIN}
cp ${SRC}/ftools/fflashprog.debug ${BIN}
cp ${SRC}/ftools/ffmemu ${BIN}
cp ${SRC}/ftools/ffmemu.debug ${BIN}
cp ${SRC}/ftools/fgpolarity ${BIN}
cp ${SRC}/ftools/fgpolarity.debug ${BIN}
cp ${SRC}/ftools/fice ${BIN}
cp ${SRC}/ftools/fice.debug ${BIN}
cp ${SRC}/ftools/flpgbtconf ${BIN}
cp ${SRC}/ftools/flpgbtconf.debug ${BIN}
cp ${SRC}/ftools/flpgbtds24 ${BIN}
cp ${SRC}/ftools/flpgbtds24.debug ${BIN}
cp ${SRC}/ftools/flpgbti2c ${BIN}
cp ${SRC}/ftools/flpgbti2c.debug ${BIN}
cp ${SRC}/ftools/flpgbtio ${BIN}
cp ${SRC}/ftools/flpgbtio.debug ${BIN}
cp ${SRC}/ftools/fpepo ${BIN}
cp ${SRC}/ftools/fpepo.debug ${BIN}
cp ${SRC}/ftools/fplayback ${BIN}
cp ${SRC}/ftools/fplayback.debug ${BIN}
cp ${SRC}/ftools/fscaadc ${BIN}
cp ${SRC}/ftools/fscaadc.debug ${BIN}
cp ${SRC}/ftools/fscads24 ${BIN}
cp ${SRC}/ftools/fscads24.debug ${BIN}
cp ${SRC}/ftools/fscadac ${BIN}
cp ${SRC}/ftools/fscadac.debug ${BIN}
cp ${SRC}/ftools/fscads24 ${BIN}
cp ${SRC}/ftools/fscads24.debug ${BIN}
cp ${SRC}/ftools/fscai2cgbtx ${BIN}
cp ${SRC}/ftools/fscai2cgbtx.debug ${BIN}
cp ${SRC}/ftools/fscai2c ${BIN}
cp ${SRC}/ftools/fscai2c.debug ${BIN}
cp ${SRC}/ftools/fscaio ${BIN}
cp ${SRC}/ftools/fscaio.debug ${BIN}
cp ${SRC}/ftools/fscajfile ${BIN}
cp ${SRC}/ftools/fscajfile.debug ${BIN}
cp ${SRC}/ftools/fscajtag ${BIN}
cp ${SRC}/ftools/fscajtag.debug ${BIN}
cp ${SRC}/ftools/fscaid ${BIN}
cp ${SRC}/ftools/fscaid.debug ${BIN}
cp ${SRC}/ftools/fscareply ${BIN}
cp ${SRC}/ftools/fscareply.debug ${BIN}
cp ${SRC}/ftools/fupload ${BIN}
cp ${SRC}/ftools/fupload.debug ${BIN}
cp ${SRC}/ftools/fttcbusy ${BIN}
cp ${SRC}/ftools/fttcbusy.debug ${BIN}
cp ${SRC}/ftools/fttcemu ${BIN}
cp ${SRC}/ftools/fttcemu.debug ${BIN}
cp ${SRC}/ftools/fuptest ${BIN}
cp ${SRC}/ftools/fuptest.debug ${BIN}


cp ${SRC}/ftools/libFlxTools.so ${LIB}/libFlxTools.so.${DOT_VERSION}
ln -s libFlxTools.so.${DOT_VERSION} ${LIB}/libFlxTools.so
cp ${SRC}/ftools/libFlxTools.so.debug ${LIB}/libFlxTools.so.debug.${DOT_VERSION}
ln -s libFlxTools.so.debug.${DOT_VERSION} ${LIB}/libFlxTools.so.debug


# hdlc_coder
( cd hdlc_coder; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r hdlc_coder/hdlc_coder ${INC}
cp ${SRC}/hdlc_coder/libhdlc_coder.so ${LIB}/libhdlc_coder.so.${DOT_VERSION}
ln -s libhdlc_coder.so.${DOT_VERSION} ${LIB}/libhdlc_coder.so
cp ${SRC}/hdlc_coder/libhdlc_coder.so.debug ${LIB}/libhdlc_coder.so.debug.${DOT_VERSION}
ln -s libhdlc_coder.so.debug.${DOT_VERSION} ${LIB}/libhdlc_coder.so.debug


# netio-next
( cd netio-next; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r ${SRC}/netio-next/include/* ${INC}
cp ${SRC}/netio-next/libnetio-lib.so ${LIB}/libnetio-lib.so.${DOT_VERSION}
ln -s libnetio-lib.so.${DOT_VERSION} ${LIB}/libnetio-lib.so
cp ${SRC}/netio-next/libnetio-lib.so.debug ${LIB}/libnetio-lib.so.debug.${DOT_VERSION}
ln -s libnetio-lib.so.debug.${DOT_VERSION} ${LIB}/libnetio-lib.so.debug
cp ${SRC}/netio-next/netio_py.so ${LIB}/netio_py.so.${DOT_VERSION}
ln -s netio_py.so.${DOT_VERSION} ${LIB}/netio_py.so
# FIXME, this probably does not work,the lib and debug have different names...
cp ${SRC}/netio-next/libnetio_py.so.debug ${LIB}/libnetio_py.so.debug.${DOT_VERSION}
ln -s libnetio_py.so.debug.${DOT_VERSION} ${LIB}/libnetio_py.so.debug
cp ${SRC}/netio-next/netio-cat ${BIN}
cp ${SRC}/netio-next/netio-cat.debug ${BIN}

mkdir -p ${TST}/netio-next/
cp -r ${SRC}/netio-next/test ${TST}/netio-next
cp ${SRC}/netio-next/netio-next-test ${TST}/netio-next/
cp ${SRC}/netio-next/netio-next-unit-tests ${TST}/netio-next/
cp ${SRC}/netio-next/pubflood ${TST}/netio-next/
cp ${SRC}/netio-next/pubfloodmt ${TST}/netio-next/
cp ${SRC}/netio-next/publish ${TST}/netio-next/
cp ${SRC}/netio-next/recv ${TST}/netio-next/
cp ${SRC}/netio-next/send ${TST}/netio-next/
cp ${SRC}/netio-next/send-buffered ${TST}/netio-next/
cp ${SRC}/netio-next/subscribe ${TST}/netio-next/
cp ${SRC}/netio-next/subscribemt ${TST}/netio-next/
cp ${SRC}/netio-next/unbuf_publish ${TST}/netio-next/
cp ${SRC}/netio-next/unbuf_subscribe ${TST}/netio-next/


# python_env
cp -r python_env/bin/supervisord ${BIN}
cp -r python_env/bin/supervisorctl ${BIN}
cp -r python_env/bin/pidproxy ${BIN}
cp -r python_env/bin/echo_supervisord_conf ${BIN}
cp -r python_env/lib/python3.11/site-packages/* ${LIB_PYTHON}


# regmap
( cd regmap; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
cp -r regmap/regmap ${INC}
cp ${SRC}/regmap/libregmap.so ${LIB}/libregmap.so.${DOT_VERSION}
ln -s libregmap.so.${DOT_VERSION} ${LIB}/libregmap.so
cp ${SRC}/regmap/libregmap.so.debug ${LIB}/libregmap.so.debug.${DOT_VERSION}
ln -s libregmap.so.debug.${DOT_VERSION} ${LIB}/libregmap.so.debug


# tdaq_tools
cp ${SRC}/tdaq_tools/bin/* ${BIN}
cp ${SRC}/tdaq_tools/lib/* ${LIB}


#
# MAKE sure to do this last, as the drivers need to be from FELIX, not from TDAQ (tools)
#
# drivers_rcc library
#( cd drivers_rcc; echo -n "${PWD##*/} " >> ../${VERSIONS}; git describe --tags --always --long >> ../${VERSIONS} )
#cp ${SRC}/drivers_rcc/libdrivers_rcc.so ${LIB}
#cp -a drivers_rcc/lib64/${TDAQ_HOST_ARCH}/* ${LIB}

#
# POST Processing
#

# make sure the x bit is set for find-elf to find them
chmod +x ${LIB}/*

echo "external libraries"
not_found=`find ${SRC} -type f -perm /a+x ! -path "./CMakeFiles/*" -exec ldd {} \; 2>&1 | grep \.so | grep /external/ | grep 'not found' | wc -l`
if test $not_found -ne 0; then
    echo "ERROR 1: Run the 'source cmake_tdaq/bin/setup.sh' before creating a distribution"
    exit 1
fi
find ${SRC} -type f -perm /a+x ! -path "./CMakeFiles/*" -exec ldd {} \; | grep \.so | grep /external/ | sed -e '/^[^\t]/ d' | sed -e 's/\t//' | sed -e 's/.*\/external\//external\//' | sed -e 's/ (0.*)//' | sort | uniq -c | sort -n > ${EXTERNAL_VERSIONS}
awk '{print $2}' ${EXTERNAL_VERSIONS} | xargs cp -t ${LIB}

echo "check dependencies from LCG"
not_found=`find ${DST} -type f -perm /a+x ! -path "./CMakeFiles/*" -exec ldd {} \; 2>&1 | grep \.so | grep /cvmfs | grep 'not found' | wc -l`
if test $not_found -ne 0; then
    echo "ERROR 2: Run the 'source cmake_tdaq/bin/setup.sh' before creating a distribution"
    exit 1
fi

echo "copy deps from LCG"
find ${DST} -type f -perm /a+x ! -path "./CMakeFiles/*" ! -path "*.py" -exec ldd {} \; | grep \.so | grep /cvmfs | sed -e '/^[^\t]/ d' | sed -e 's/\t//' | sed -e 's/.*=..//' | sed -e 's/ (0.*)//' | sort | uniq -c | sort -n > ${CVMFS_VERSIONS}
awk '{print $2}' ${CVMFS_VERSIONS} | xargs cp -t ${LIB} || true

echo "fix or setup rpath"
./find-elf.sh ${BIN} | grep -v platforms | xargs -n 1 ${PATCHELF} --set-rpath '$ORIGIN/../lib' --force-rpath
./find-elf.sh ${LIB} | xargs -n 1 ${PATCHELF} --set-rpath '$ORIGIN' --force-rpath
./find-elf.sh ${BIN}/platforms | xargs -n 1 ${PATCHELF} --set-rpath '$ORIGIN/../../lib' --force-rpath
./find-elf.sh ${TST} | xargs -n 1 ${PATCHELF} --set-rpath '$ORIGIN/../../lib' --force-rpath
${PATCHELF} --remove-rpath ${BIN}/fflash

echo "internal consistency (missing felix libraries) for binaries"
not_found=`./find-elf.sh ${BIN} | xargs ldd | grep -v 'libboost' | grep -v 'libyaml' | grep -v 'libtbb' | grep 'not found' | wc -l`
if test $not_found -ne 0; then
    libs_not_found=`./find-elf.sh ${BIN} | xargs ldd | grep 'not found'`
    echo "ERROR 0: missing library for one of the binaries: $libs_not_found"
    exit 1
fi

echo "internal consistency (missing felix libraries) for tests"
not_found=`./find-elf.sh ${TST} | xargs ldd | grep -v 'libboost' | grep -v 'libyaml' | grep -v 'libtbb' | grep 'not found' | wc -l`
if test $not_found -ne 0; then
    libs_not_found=`./find-elf.sh ${TST} | xargs ldd | grep 'not found'`
    echo "ERROR 0: missing library for one of the tests: $libs_not_found"
    exit 1
fi

echo "cleanup files"
sort -o ${VERSIONS} ${VERSIONS}
cat cmake_tdaq/cmake/modules/FELIX-versions.cmake >> ${VERSIONS}

echo "pack it all up"
# BRANCH=`git rev-parse --abbrev-ref HEAD`
DISTRIBUTION=${DIST_NAME}-${BINARY_TAG}
tar zcvf ${DST_DIR}/${DISTRIBUTION}.tar.gz -C ${TARGET_DIR} ${DIST_NAME}/README.md ${DIST_NAME}/${BINARY_TAG}
