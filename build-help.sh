#!/bin/bash  -x
set -e

TAG=$1

HELP=${TAG}-${BINARY_TAG}-help
mkdir -p ${HELP}

for name in flx-busy-mon flx-config flx-dma-stat flx-i2c flx-info flx-init flx-irq-test flx-pod flx-reset; do
  $BINARY_TAG/flxcard/${name} -h > ${HELP}/${name}.txt
done

for name in fcap fcheck fdaq fdaqm febrc fec feconf fedma fedump fela felink femu femuran fereverse feto fexoff fexofftx fflash fflashprog ffmemu fgbtxconf fgpolarity fice flpgbtconf flpgbtds24 flpgbti2c flpgbtio fpepo fplayback fscaadc fscadac fscads24 fgbtxconf fscai2c fscai2cgbtx fscaio fscajfile fscajtag fscaid fscareply fttcbusy fttcemu fupload fxvcserver; do
  $BINARY_TAG/ftools/${name} -h > ${HELP}/${name}.txt
done

for name in felig-config; do  # felig-start felig-stop felig-status felig-reset
  $BINARY_TAG/felig-tools/${name} -h > ${HELP}/${name}.txt
done

#
# for regmap 4 only
#
#if [ "$REGMAP_VERSION" == "0x0400" ]; then
  for name in felix-buslist; do
    $BINARY_TAG/felixbus-client/${name} -h > ${HELP}/${name}.txt
  done

  for name in felixcore; do
    $BINARY_TAG/felixcore/${name} -h > ${HELP}/${name}.txt
  done
#fi

# felix-cmem-free felix-display-stats felix-fid felix-get-mode felix-register-simulator felix-test-send-toflx-buffered felix-test-swrod-buffered felix-test-swrod-unbuf
for name in felix-elink2file felix-fifo2elink felix-file2host felix-get-ip felix-register felix-register-client felix-test-swrod felix-toflx felix-toflx2file felix-tohost; do
   $BINARY_TAG/felix-star/${name} --help > ${HELP}/${name}.txt
done

# output of fid command
# FIXME re-add
#$BINARY_TAG/felix-star/felix-fid --verbose 0x123456789abc3def > ${HELP}/fid.txt

$BINARY_TAG/felix-starter/felix-supervisord-generate --help > ${HELP}/felix-supervisord-generate.txt

# extra example files
cp felix-starter/etc/felix-tbed.cfg ${HELP}/
cp felix-interface/felix/felix_client_thread_interface.hpp ${HELP}/
cp felix-interface/felix/felix_client_thread_extension.hpp ${HELP}/
cp felix-interface/felix/felix_client_thread_extension42.hpp ${HELP}/
cp felix-interface/felix/felix_client_thread_extension421.hpp ${HELP}/

tar zcvf ${HELP}.tar.gz ${HELP}
