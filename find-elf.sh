#!/bin/sh

usage()
{
    echo "Usage: $0 [-L] directory ..." >&2
    exit 1
}

L=

case $1 in
-L)
    # follow symlinks
    L=$1
    shift
esac

[ $# = 0 ] && usage

# avoid patching fflash (statically linked), but not fflashprog
find $L "$@" -type f -perm /a+x -exec sh -c 'file '"$L"' "$0" | grep -q "ELF" && echo "$0"' {} \; | grep -v \\.debug | grep -v -e "/fflash$"
