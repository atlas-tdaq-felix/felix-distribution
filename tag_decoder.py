import re

default_prefix = 'felix'
regmap4_version = '4.2.0'
regmap5_version = '5.0.0'


def prefix_major_minor_patch_rc_rm(tag):

    regmap_regexp = r"^0x?(?P<major>\d{2})(?P<minor>\d{2})$"
    dot_regexp = r"^(?:(?P<prefix>\D.*?)-)?(?P<major>\d{1,2})\.(?P<minor>\d{1,2})(?:\.(?P<patch>[x\d]{1,2}))?(?:\.(?P<rc>\D.*))?$"
    dash_regexp = r"^(?:(?P<prefix>\D.*?)-)?(?P<major>\d{1,2})\-(?P<minor>\d{1,2})(?:\-(?P<patch>[x\d]{1,2}))?(?:\-(?P<rc>\D.*))?$"
    git_describe_regexp = r"^(?:(?P<prefix>\D.*?)-)?(?P<major>\d{1,2})\-(?P<minor>\d{1,2})(?:\-(?P<patch>[x\d]{1,2}))?(?:\-(?P<rc>\D.*))?-(?P<sequence>\d+)-g(?P<hash>[0-9a-zA-Z]{7})$"

    m = re.match(regmap_regexp, tag)
    if m:
        # Adjust versions
        if int(m.group('major')) == 4:
            return prefix_major_minor_patch_rc_rm(regmap4_version)

        return prefix_major_minor_patch_rc_rm(regmap5_version)

    if tag.endswith('-rm4'):
        rm = 'rm4'
        tag = tag[:-4]
    elif tag.endswith('-rm5'):
        rm = 'rm5'
        tag = tag[:-4]
    else:
        rm = None

    m = re.match(git_describe_regexp, tag)
    if m:
        prefix = m.group('prefix') if m.group('prefix') else default_prefix
        patch = int(m.group('patch')) if m.group('patch') and not m.group('patch').startswith('x') else -1
        rc = m.group('rc')
        if int(m.group('sequence')) > 0:
            patch = patch + 1 if patch >= 0 and rc is None else patch
        return (prefix, int(m.group('major')), int(m.group('minor')), patch, rc, rm)

    m = re.match(dot_regexp, tag)
    if m:
        prefix = m.group('prefix') if m.group('prefix') else default_prefix
        patch = int(m.group('patch')) if m.group('patch') and not m.group('patch').startswith('x') else -1
        return (prefix, int(m.group('major')), int(m.group('minor')), patch, m.group('rc'), rm)

    m = re.match(dash_regexp, tag)
    if m:
        prefix = m.group('prefix') if m.group('prefix') else default_prefix
        patch = int(m.group('patch')) if m.group('patch') and not m.group('patch').startswith('x') else -1
        return (prefix, int(m.group('major')), int(m.group('minor')), patch, m.group('rc'), rm)

    prefix = default_prefix
    rc = tag[len(default_prefix + '-'):] if tag.startswith(default_prefix + '-') else tag

    return (prefix, 0, 0, 0, rc, rm)


def prefix_dot_dash(tag):

    # Examples
    #
    # master
    # master-latest
    # felix-master
    # felix-master-00-04-02
    # felix-00-04-02
    # felix-master-00-04-02-rc3
    # felix-00-04-02-rc3
    # 00-04-02
    # 00-04-02-rc3
    # felix-00-04
    # felix-master-00-04
    # 00-04

    (prefix, major, minor, patch, rc, rm) = prefix_major_minor_patch_rc_rm(tag)
    # print((prefix, major, minor, patch, rc, rm, tag))
    if major + minor + patch == 0:
        dot = rc + ('-' + rm if rm else '')
        dash = rc + ('-' + rm if rm else '')
    else:
        dot = str(major) + '.' + str(minor) + '.' + (str(patch) if patch >= 0 else 'x') + ('.' + rc if rc else '') + ('-' + rm if rm else '')
        dash = str(major).rjust(2, '0') + '-' + str(minor).rjust(2, '0') + '-' + (str(patch).rjust(2, '0') if patch >= 0 else 'xx') + ('-' + rc if rc else '') + ('-' + rm if rm else '')

    # print((prefix, dot, dash))

    return (prefix, dot, dash)
