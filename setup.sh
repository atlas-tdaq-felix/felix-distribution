# Usage: source setup.sh
export FI_VERBS_TX_IOV_LIMIT=30
export FI_VERBS_RX_IOV_LIMIT=30
export FI_VERBS_TX_SIZE=1024
export FI_VERBS_RX_SIZE=1024
export RDMAV_FORK_SAFE=1

BASEDIR=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

source $BASEDIR/python_env/bin/activate
source $BASEDIR/cmake_tdaq/bin/setup.sh
